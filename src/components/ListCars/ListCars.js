import { connect } from 'react-redux';

function ListCars(props) {
  const {cars} = props; 
  return (
    <ul>
      {cars.items.map((item) => <li key={item.id} >{`${item.name} ${item.price}`}</li>)}
    </ul>
  );
}

const mapStateToProps = ( state ) => ({ 
  cars: state.cars    
})

export default connect(mapStateToProps)(ListCars);
