import { connect } from 'react-redux';
import './UserHolder.scss';

function UserHolder({ user }) {
  const now = new Date();
  const year = now.getFullYear();
  const yearOfBirth = year - user.age;
  return (
    <div className={user?.age < 18 ? 'red' : 'blue'}>{user.name} {user.lastName} {yearOfBirth}</div>
  );
}

const mapStateToProps = ( state ) => ({ 
  user: state.user    
})

export default connect(mapStateToProps)(UserHolder);
