import React, { Component } from 'react';
import { connect } from 'react-redux';
import './CommonName.scss';

class CommonName extends Component {
  getName() {
    const {cars, user} = this.props;
    let result = 'нет';
    cars.items.forEach((car) => {
      if (car.name === user.name) { 
        result = 'да';
      }  
        
    })
    return result;
  }
  render() {
    return (
      <div className="circle">{this.getName() }</div>
    )
  }
};

const mapStateToProps = ({ cars, user }) => ({
  cars,
  user,
})

export default connect(mapStateToProps)(CommonName);
