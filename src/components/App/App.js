import InputHolder from "../InputHolder/InputHolder"
import UserHolder from '../UserHolder/UserHolder';
import ListCars from '../ListCars/ListCars';
import CommonName from '../CommonName/CommonName';

function App() {
  return (
      <main>
        <UserHolder />
        <InputHolder />
        <ListCars />
        <CommonName />
      </main>
  );
}

export default App;
