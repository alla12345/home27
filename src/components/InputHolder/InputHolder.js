import React, { Component } from 'react';
import { connect } from 'react-redux';
import { updateUser } from "../../actionCreators"

class InputHolder extends Component {
  state = {
    value: "",
  }

  handleChange = (e) => {
    this.setState({ value: Number(e.target.value)});//неправильно
  }

  handleClick = () => {
    const { value } = this.state;
    const { updateUser } = this.props;

    updateUser(value); 
    this.setState({ value: '' });
  }

  render() {
    const { value } = this.state
    return (
      <div className="wrapper">
        <input value={value} onChange={this.handleChange} />
        <button disabled={!value} onClick={this.handleClick}>изменить</button>
      </div>
    )
  }
};

const mapDispatchToProps = dispatch => ({ 
  updateUser: age => dispatch(updateUser(age))
})

export default connect(null, mapDispatchToProps)(InputHolder);
