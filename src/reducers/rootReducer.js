import { combineReducers } from 'redux';
import cars from './carsReducer';
import user from './userReducer';

export default combineReducers({
    user,
    cars,
});
