const initState = { 
  items: [
    {id: 0, name: 'BMW', price: 5000},
    {id: 1, name: 'Lada', price: 200},
    {id: 2, name: 'Ford', price: 1300},
  ] 
};

function reducer(state = initState, action) { 
  switch(action.type) {

    default: return state; 
  }
}

export default reducer;
