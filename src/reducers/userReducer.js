const initState = {
  name: 'Lada',
  lastName: 'Petrova',
  age: 20,
}

function reducer(state = initState, action) {
  switch(action.type) {
    case "UPDATE_USER": { 
      return { ...state, age: action.payload }
    }
    default: return state;
  }
}

export default reducer

